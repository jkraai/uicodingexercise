# Set up
1. ```npm i```
2. ```npm start```

Works with Node 8.11

### Notes
To emulate an actual api the app doesn't resolve the sample data for a random short amount of time.

**note: The "entries" api endpoint is set to "fail" 50% of the time to demonstrate some basic error handling should an actual api be unavailable.**
Click a few times & the entries will load (sometimes several failures occur in a row).