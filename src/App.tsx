import * as React from 'react';
import { Provider } from "react-redux";
import './App.css';
import Router from "./components/Router";
import { store } from "./store";

class App extends React.Component {
  public render() {
    return (
      <Provider store={store}>
        <div className="App" style={{display: 'flex'}}>
          <Router />
        </div>
      </Provider>
    );
  }
}

export default App;
