import { Drawer, List, ListItem, ListItemText } from "@material-ui/core";
import { StyledComponentProps, StyleRulesCallback, withStyles } from "@material-ui/core/styles";
import { Map } from "immutable";
import * as React from "react";
import { connect } from "react-redux";
import { BrowserRouter, Link, Route } from "react-router-dom";
import { ActionCreator } from "redux";
import { ISetActiveSpace, setActiveSpace } from "../store/actions";
import { getSpaces } from "../store/selectors";
import { StateRecord } from "../store/state";
import { SpaceRecord } from "../store/types";
import Space from "./Space";

interface IProps extends StyledComponentProps {
  spaces: Map<string, SpaceRecord>;
  setActiveSpace: ActionCreator<ISetActiveSpace>;
}

const styles: StyleRulesCallback = (theme) => ({
  drawerPapaer: {
    position: 'relative',
    width: 240,
  },
  root: {
    flexGrow: 1,
  },
});

export const Router = (props: IProps) => (
  <BrowserRouter>
    <main className={props.classes!.root} style={{display: 'flex'}}>
      <Drawer variant={"permanent"} anchor={"left"} classes={{paper: props.classes!.drawerPaper}} id={"nav"}>
        <List>
          {props.spaces.valueSeq().map((space: SpaceRecord): any => {
            const onClick = () => props.setActiveSpace(space.get("id"));
            return (
                <Link to={`/${space.get("id")}`} key={space.get("id")!} onClick={onClick}>
                  <ListItem button={true}>
                    <ListItemText primary={space.get("name")}/>
                  </ListItem>
                </Link>
            )
          })}
        </List>
      </Drawer>
      <Route path="/:id" component={Space}/>
    </main>
  </BrowserRouter>
);

const mapStateToProps = (state: StateRecord): any => ({
  spaces: getSpaces(state),
});

const mapDispatchToProps = {
  setActiveSpace,
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Router as any));
