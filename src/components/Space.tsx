import { Button, Paper, Tab, Table, TableBody, TableHead, TableRow, Tabs } from "@material-ui/core";
import  { ReportProblem, RotateRight } from "@material-ui/icons";
import { List, Map } from "immutable";
import * as React from "react";
import { connect } from "react-redux";
import { RouteComponentProps } from "react-router-dom";
import { ActionCreator } from "redux";
import { ISortColumn, IStart, IToggleLoading, sortColumn, start, toggleLoading } from "../store/actions";
import { getSortedRecords, getSortOptions, getSpaces, RecordTypes } from "../store/selectors";
import { EntryRecord, SortOptionsRecord, SpaceRecord } from "../store/types";
import { TableBodyRow } from "./TableBodyRow";
import TableHeaderCell from "./TableHeaderCell";


interface IProps extends RouteComponentProps {
  spaces: Map<string, SpaceRecord>;
  records: List<RecordTypes>;
  sortColumn: ActionCreator<ISortColumn>;
  sortOptions: SortOptionsRecord;
  errors: any;
  start: ActionCreator<IStart>;
  toggleLoading: ActionCreator<IToggleLoading>;
  loading: boolean;
}

const columns = {
  entryRecords: ["Title", "Summary", "Created By", "Updated By", "Last Updated"],
  assets: ["Title", "Content Type", "File Name", "Created By", "Updated By", "Last Updated"]
}

const tabIndices = ["entryRecords", "assets"];

export const Space = (props: IProps) => {
  const params = props.match.params as any;
  const spaceRecord = props.spaces.get(params.id);
  const onClick = (e: any, value: any) => {
    props.sortColumn(tabIndices[value], "title");
  };

  const headings = columns[props.sortOptions.get("table")];
  const reload = () => {
    props.toggleLoading();
    props.start();
  };
  return (
    <Paper style={{flexGrow: 1}}>
      <h1 className={'space-title'}>{spaceRecord && spaceRecord.get("name")}</h1>
      <h5>created by {spaceRecord && spaceRecord.get("createdBy")}</h5>
      <p>{spaceRecord && spaceRecord.get("description")}</p>
      {tabIndices.indexOf(props.sortOptions.get("table")) === 0 && props.errors.get(params.id) && props.errors.get(params.id).get("entries") && <div>
            <ReportProblem/><p>Error getting entries</p>
            <Button disabled={props.loading} color="secondary" variant="contained" onClick={reload}>Please try again</Button>
            </div>}
      {props.loading && <RotateRight className={"loading"}/>}
      <Tabs indicatorColor="primary" textColor="primary" value={tabIndices.indexOf(props.sortOptions.get("table"))} onChange={onClick}>
        <Tab label="Entries" />
        <Tab label="Assets" />
      </Tabs>
      <Table>
        <TableHead>
          <TableRow>
            {headings.map((column: string): any => (<TableHeaderCell column={column} key={column}/>))}
          </TableRow>
        </TableHead>
        <TableBody>
          {props.records.valueSeq().map((record: EntryRecord, key: number): any => (<TableBodyRow key={key} record={record}/>))}
        </TableBody>
      </Table>
    </Paper>
  );
};

const mapStateToProps = (state: any): any => ({
  records: getSortedRecords(state),
  spaces: getSpaces(state),
  sortOptions: getSortOptions(state),
  errors: state.errors,
  loading: state.loading,
});

const mapDispatchToProps = {
  sortColumn,
  start,
  toggleLoading,
};

export default connect(mapStateToProps, mapDispatchToProps)(Space);