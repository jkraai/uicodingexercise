import { TableCell, TableRow } from "@material-ui/core";
import { Record } from "immutable";
import { isMoment } from "moment";
import * as React from "react";

interface IProps {
  record: Record<any>;
}

export const TableBodyRow = (props: IProps) => {
  return (
    <TableRow>
      {Object.keys(props.record.toSeq().toMap().toObject()).map((key) => {
        let val = props.record.get(key);
        if (isMoment(val)) {
          val = val.format('M-DD-YYYY');
        }
        return (<TableCell key={key as any}>{val}</TableCell>)
      })}
    </TableRow>
  );
};
