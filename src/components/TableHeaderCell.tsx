import { TableCell, TableSortLabel, Tooltip } from "@material-ui/core";
import * as React from "react";
import { connect } from "react-redux";
import { ActionCreator } from "redux";
import { ISortColumn, sortColumn } from "../store/actions";
import { getSortOptions } from "../store/selectors";
import { StateRecord } from "../store/state";
import { SortOptionsRecord } from "../store/types";

interface IProps {
  column: string;
  sortColumn: ActionCreator<ISortColumn>;
  sortOptions: SortOptionsRecord;
}

export const TableHeaderCell = (props: IProps) => {
  const onClick = () => {
    props.sortColumn(props.sortOptions.get("table"), props.column)
  };

  const order = props.column === props.sortOptions.get("column") ? props.sortOptions.get("order") : "desc";

  return (
    <TableCell key={props.column}>
      <Tooltip title="Sort" enterDelay={300} placement={"bottom-end"}>
        <TableSortLabel direction={order} onClick={onClick} active={true}>
          {props.column}
        </TableSortLabel>
      </Tooltip>
    </TableCell>
  );
};

const mapStateToProps = (state: StateRecord) => ({
  sortOptions: getSortOptions(state),
});

const mapDispatchToProps = {
  sortColumn,
};

export default connect(mapStateToProps, mapDispatchToProps)(TableHeaderCell as any) as any;
