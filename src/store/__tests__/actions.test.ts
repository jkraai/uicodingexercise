import { ActionTypes, setActiveSpace, sortColumn, start, toggleLoading } from "../actions";
import { Table } from "../types";

describe("action creators", () => {
  describe("sort column", () => {
    it("should return an action", () => {
      const expected = {
        column: "test",
        table: Table.ASSETS,
        type: ActionTypes.SORT_COLUMN,
      };

      expect(sortColumn(Table.ASSETS, "test")).toEqual(expected);
    });
  });
  describe("start", () => {
    it("should return an action", () => {
      expect(start().type).toBeTruthy();
    });
  });
  describe("toggle loading", () => {
    it("should return an action", () => {
      expect(toggleLoading().type).toBeTruthy();
    });
  });
  describe("set active space", () => {
    it("should have a space id", () => {
      expect(setActiveSpace("1").id).toBeTruthy();
    });
  });
});
