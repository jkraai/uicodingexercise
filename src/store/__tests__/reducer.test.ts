import { addAsset, addEntry, addSpace, setActiveSpace, sortColumn, toggleLoading } from "../actions";
import { reducer } from "../reducer";
import { State, StateRecord } from "../state";
import { Asset, Entry, Space, Table } from '../types';

describe("reducer", () => {
  let initialState: StateRecord;

  beforeEach(()=> {
    initialState = new State();
  });

  it("should pass through unhandled actions", () => {
    const unhandledAction = {
      type: "test",
    };

    const newState = reducer(initialState, unhandledAction);

    expect(newState).toEqual(initialState);
  });
  it("should set sort options", () => {
    const action = sortColumn(Table.ASSETS, "test");
    const newState = reducer(initialState, action);

    expect(newState.getIn(["sortOptions", "column"])).toEqual("test");
    expect(newState.getIn(["sortOptions", "table"])).toEqual(Table.ASSETS);
  });
  it("should toggle loading", () => {
    const action = toggleLoading();
    const newState = reducer(initialState, action);

    expect(newState.get("loading")).toBe(false);
  });
  it("should set the active space", () => {
    const action = setActiveSpace("test");
    const newState = reducer(initialState, action);

    expect(newState.get("activeSpace")).toEqual("test");
  });
  it("should add a space", () => {
    const testSpace = new Space({ name: "test" });
    const action = addSpace("1234", testSpace);

    const newState = reducer(initialState, action);

    expect(newState.get("spaces").size).toBe(1);
    expect(newState.getIn(["spaces", "1234", "name"])).toBe("test");
  });
  it("should add an entry to an existing space", () => {
    const testSpace = new Space({ name: "test" });
    let state = reducer(initialState, addSpace("1234", testSpace));
    const entry = new Entry({ title: "test entry" })
    state = reducer(state, addEntry("1234", entry));

    expect(state.getIn(["spaces", "1234"]).get("entryRecords").size).toBe(1);
    expect(state.getIn(["spaces", "1234"]).getIn(["entryRecords", 0, "title"])).toBe("test entry");
  });
  it("should add an asset to an existing space", () => {
    const testSpace = new Space({ name: "test" });
    let state = reducer(initialState, addSpace("1234", testSpace));
    const asset = new Asset({ title: "test asset" })
    state = reducer(state, addAsset("1234", asset));

    expect(state.getIn(["spaces", "1234"]).get("assets").size).toBe(1);
    expect(state.getIn(["spaces", "1234"]).getIn(["assets", 0, "title"])).toBe("test asset");
  });
});