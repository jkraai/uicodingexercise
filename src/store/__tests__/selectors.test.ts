import { List, Map } from "immutable";
import * as moment from "moment";
import * as selectors from "../selectors";
import { State, StateRecord } from "../state";
import { Entry, SortOptions, Space } from "../types";

describe("selectors", () => {
  let initialState: StateRecord;

  beforeEach(() => {
    initialState = new State();
  });

  describe("getActiveSpace", () => {
    it("should return null on new state", () => {
      expect(selectors.getActiveSpace(initialState)).toBe(null);
    });
    it("should return the active Space", () => {
      const state = initialState.set("activeSpace", "test");

      expect(selectors.getActiveSpace(state)).toBe("test");
    });
  });

  describe("getSortOptions", () => {
    it("should return an empty record for a new state", () => {
      expect(selectors.getSortOptions(initialState)).toEqual(new SortOptions());
    });
    it("should return set sort options", () => {
      const state = initialState.setIn(["sortOptions", "column"], "test");

      expect(selectors.getSortOptions(state).get("column")).toEqual("test");
    });
  });

  describe("getSortedRecords", () => {
    it("should return an empty list when no space is selected", () => {
      expect(selectors.getSortedRecords(initialState).size).toBe(0);
    });
    it("should return sorted asc by title entries by default", () => {
      const space = new Space().set("entryRecords", List([
        new Entry({
          title: "C"
        }),
        new Entry({
          title: "B",
        }),
        new Entry({
          title: "A"
        })
      ]));

      const state = initialState.set("activeSpace", "test")
        .set("spaces", Map({
          "test": space,
        }));
      
      expect(selectors.getSortedRecords(state).get(0)!.get("title")).toEqual("A");
      expect(selectors.getSortedRecords(state).get(1)!.get("title")).toEqual("B");
      expect(selectors.getSortedRecords(state).get(2)!.get("title")).toEqual("C");
    });
    it("should return entries last updated most recently by default", () => {
      const space = new Space().set("entryRecords", List([
        new Entry({
          lastUpdated: moment("20150101", "YYYYMMDD"),
        }),
        new Entry({
          lastUpdated: moment("20160101", "YYYYMMDD"),
        }),
        new Entry({
          lastUpdated: moment("20170101", "YYYYMMDD"),
        })
      ]));

      const state = initialState.set("activeSpace", "test")
        .set("spaces", Map({ test: space }))
        .setIn(["sortOptions", "column"], "lastUpdated");
      
        expect(selectors.getSortedRecords(state).get(0)!.get("lastUpdated")!.format("YYYY")).toEqual("2017");
        expect(selectors.getSortedRecords(state).get(1)!.get("lastUpdated")!.format("YYYY")).toEqual("2016");
        expect(selectors.getSortedRecords(state).get(2)!.get("lastUpdated")!.format("YYYY")).toEqual("2015");
    });
  });
});
