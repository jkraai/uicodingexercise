import { State } from "../state";

describe("State", () => {
  it("should be loading by default", () => {
    expect(new State().get("loading")).toBe(true);
  });
});
