import { AnyAction } from "redux";
import { AssetRecord, EntryRecord, SpaceRecord, Table } from "./types";

/**
 * Action Types
 */
export enum ActionTypes {
  SORT_COLUMN = "SORT_COLUMN",
  START = "START",
  SET_ACTIVE_SPACE = "SET_ACTIVE_SPACE",
  TOGGLE_LOADING = "TOGGLE_LOADING",
  ADD_SPACE = "ADD_SPACE",
  ADD_ENTRY = "ADD_ENTRY",
  ADD_ASSET = "ADD_ASSET",
  SET_ERROR = "SET_ERROR",
}

/**
 * Actions
 */
export interface ISortColumn extends AnyAction {
  column: string;
  table: Table;
  type: ActionTypes.SORT_COLUMN;
}

export interface IStart extends AnyAction {
  type: ActionTypes.START;
}

export interface IToggleLoading extends AnyAction {
  type: ActionTypes.TOGGLE_LOADING;
}

export interface ISetActiveSpace extends AnyAction {
  id: string;
  type: ActionTypes.SET_ACTIVE_SPACE;
}

export interface IAddSpace extends AnyAction {
  id: string;
  space: SpaceRecord;
  type: ActionTypes.ADD_SPACE;
}

export interface IAddEntry extends AnyAction {
  entry: EntryRecord;
  spaceId: string;
  type: ActionTypes.ADD_ENTRY;
}

export interface IAddAsset extends AnyAction {
  asset: AssetRecord;
  spaceId: string;
  type: ActionTypes.ADD_ASSET;
}

export interface ISetError extends AnyAction {
  name: string;
  value: string | null;
  spaceId: string;
  type: ActionTypes.SET_ERROR;
}

export type Actions = ISortColumn |
  IStart |
  IToggleLoading |
  ISetActiveSpace |
  IAddSpace |
  IAddAsset |
  IAddEntry |
  ISetError |
  AnyAction;

/**
 * Actions Creators
 */
export const sortColumn = (table: Table, column: string): ISortColumn => ({
  column,
  table,
  type: ActionTypes.SORT_COLUMN,
});

export const start = (): IStart => ({
  type: ActionTypes.START,
});

export const toggleLoading = (): IToggleLoading => ({
  type: ActionTypes.TOGGLE_LOADING,
});

export const setActiveSpace = (id: string): ISetActiveSpace => ({
  id,
  type: ActionTypes.SET_ACTIVE_SPACE,
});

export const addSpace = (id: string, space: SpaceRecord): IAddSpace => ({
  id,
  space,
  type: ActionTypes.ADD_SPACE,
});

export const addEntry = (spaceId: string, entry: EntryRecord): IAddEntry => ({
  entry,
  spaceId,
  type: ActionTypes.ADD_ENTRY,
});

export const addAsset = (spaceId: string, asset: AssetRecord): IAddAsset => ({
  asset,
  spaceId,
  type: ActionTypes.ADD_ASSET,
})

export const setError = (name: string, value: string | null, spaceId: string): ISetError => ({
  name,
  value,
  spaceId,
  type: ActionTypes.SET_ERROR
});
