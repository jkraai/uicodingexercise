import * as assets from "./assets.json";
import * as entries from "./entries.json";
import * as spaces from "./spaces.json";
import * as users from "./users.json";

/**
 * @link https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/random
 */
const getRandomInt = (max: number): number => {
  return Math.floor(Math.random() * Math.floor(max));
}

export const getSpaces = () => new Promise((resolve) => {
  setTimeout(() => resolve(spaces), getRandomInt(500));
});

export const getEntries = (spaceId: string) => new Promise((resolve, reject) => {
  setTimeout(() => {
    if (getRandomInt(500) > 250) {
      resolve(spaceId === "yadj1kx9rmg0" ? entries as any : null)
    } else {
      reject({ success: false, message: 'Error getting entries' });
    }
  }, getRandomInt(1000));
});

export const getAssets = (spaceId: string) => new Promise((resolve) => {
  setTimeout(() => resolve(spaceId === "yadj1kx9rmg0" ? assets as any : null), getRandomInt(1000));
});

export const getUser = (userId: string) => new Promise((resolve, reject) => {
  const userRes = users as any;
  let user;
  userRes.items.forEach((item: any) => {
    if (item.sys.id === userId) {
      user = item;
    }

    return;
  });

  if (user) {
    resolve(user);
  }
  reject({
    success: false,
    message: 'User not found',
  })
});
