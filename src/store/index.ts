import { applyMiddleware, createStore } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import createSagaMiddleware from "redux-saga";
import { start } from "./actions";
import { reducer } from "./reducer";
import sagas from "./sagas";
import { State } from "./state";

const sagaMiddlware = createSagaMiddleware();

export const store = createStore(
  reducer,
  new State(),
  composeWithDevTools(applyMiddleware(sagaMiddlware)),
);

sagaMiddlware.run(sagas);

store.dispatch(start());
