import { Actions, ActionTypes } from "./actions";
import { State, StateRecord } from "./state";
import { Order } from "./types";

export const reducer = (state: StateRecord = new State(), action: Actions): StateRecord => {
  switch (action.type) {
    case ActionTypes.SORT_COLUMN:
      const origCol = state.getIn(["sortOptions", "column"]);
      return state.setIn(["sortOptions", "column"], action.column)
        .setIn(["sortOptions", "table"], action.table)
        .setIn(["sortOptions", "order"], state.getIn(["sortOptions", "order"]) === Order.ASC && origCol === action.column ? Order.DESC : Order.ASC);
    case ActionTypes.TOGGLE_LOADING:
      return state.set("loading", !state.get("loading"));
    case ActionTypes.SET_ACTIVE_SPACE:
      return state.set("activeSpace", action.id);
    case ActionTypes.ADD_SPACE:
      return state.set("spaces", state.get("spaces").set(action.id, action.space));
    case ActionTypes.ADD_ENTRY:
      return state.setIn(
        ["spaces", action.spaceId, "entryRecords"],
        state.getIn(["spaces", action.spaceId, "entryRecords"]).push(action.entry)
      );
    case ActionTypes.ADD_ASSET:
      return state.setIn(
        ["spaces", action.spaceId, "assets"],
        state.getIn(["spaces", action.spaceId, "assets"]).push(action.asset)
      );
    case ActionTypes.SET_ERROR:
        if (state.getIn(["errors", action.spaceId, action.name]) && action.value === null) {
          return state.deleteIn(["errors", action.spaceId, action.name]);
        }

        return state.setIn(["errors", action.spaceId, action.name], action.value);
    default:
      return state;
  }
};
