import * as moment from "moment";
import { all, call, put } from "redux-saga/effects";
import { addAsset } from "../actions";
import * as api from "../api";
import { Asset } from "../types";

export default function*(spaceId: string): any {
  const data = yield call(api.getAssets, spaceId);

  if (data === null) {
    return;
  }

  for (const asset of data.items) {
    const { createdUser, updatedUser } = yield all({
      createdUser: call(api.getUser, asset.sys.createdBy),
      updatedUser: call(api.getUser, asset.sys.updatedBy),
    });
    const assetRecord = new Asset({
      contentType: asset.fields.contentType,
      createdBy: createdUser ? createdUser.fields.name : null,
      fileName: asset.fields.fileName,
      lastUpdated: moment(asset.sys.updatedAt),
      title: asset.fields.title,
      updatedBy: updatedUser ? updatedUser.fields.name : null,
    });

    yield put(addAsset(spaceId, assetRecord));
  }
}
