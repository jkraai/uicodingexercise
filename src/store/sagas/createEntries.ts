import * as moment from "moment";
import { all, call, put } from "redux-saga/effects";
import { addEntry, setError } from "../actions";
import * as api from "../api";
import { Entry } from "../types";
export default function*(spaceId: string): any {
  try {
    const data = yield call(api.getEntries, spaceId);
    yield put(setError("entries", null, spaceId));

    if (data === null) {
      return;
    }

    for (const entry of data.items) {
      const { createdUser, updatedUser } = yield all({
        createdUser: call(api.getUser, entry.sys.createdBy),
        updatedUser: call(api.getUser, entry.sys.updatedBy),
      });
      const entryRecord = new Entry({
        createdBy: createdUser ? createdUser.fields.name : null,
        lastUpdated: moment(entry.sys.updatedAt),
        summary: entry.fields.summary,
        title: entry.fields.title,
        updatedBy: updatedUser ? updatedUser.fields.name : null,
      });

      yield put(addEntry(spaceId, entryRecord));
    }
  } catch (e) {
    yield all([
      put(setError("entries", "Error getting entries", spaceId)),
    ]);
  }
}