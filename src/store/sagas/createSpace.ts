import { all, call, put } from "redux-saga/effects";
import { addSpace } from "../actions";
import * as api from "../api";
import { Space } from "../types";
import createAssets from "./createAssets";
import createEntries from "./createEntries";
export default function*(data: any): any{

  const userName = yield call(api.getUser, data.sys.createdBy);

  const space = new Space({
    createdBy: userName ? userName.fields.name : null,
    description: data.fields.description,
    id: data.sys.id,
    name: data.fields.title,
  }); 

  yield put(addSpace(data.sys.id, space));
  yield all([
    call(createEntries, data.sys.id),
    call(createAssets, data.sys.id),
  ]);
}