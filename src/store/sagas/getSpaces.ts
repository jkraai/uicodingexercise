import { all, call, put } from "redux-saga/effects";
import {  setActiveSpace, toggleLoading } from "../actions";
import * as api from "../api";
import createSpace from "./createSpace";

export default function*() {
  const spaces = yield call(api.getSpaces);

  yield all(spaces.items.map((space: any) => call(createSpace, space)));

  yield all([
    put(setActiveSpace(spaces.items[0].sys.id)),
    put(toggleLoading()),
  ])
};