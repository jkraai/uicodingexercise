import { takeLatest } from "redux-saga/effects";
import { ActionTypes } from "../actions";
import getSpaces from "./getSpaces";

export default function*() {
  yield takeLatest(ActionTypes.START, getSpaces);
}
