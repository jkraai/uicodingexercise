import * as camelCase from "camelcase";
import { List, Map } from "immutable";
import { isMoment } from "moment";
import { createSelector } from "reselect";
import { StateRecord } from "./state";
import { AssetRecord, EntryRecord, Order, SortOptionsRecord, SpaceRecord } from "./types";

export type RecordTypes = AssetRecord | EntryRecord;

export const getSpaces = (state: StateRecord): Map<string, SpaceRecord> => state.get("spaces");
export const getActiveSpace = (state: StateRecord): string | null => state.get("activeSpace");
export const getSortOptions = (state: StateRecord): SortOptionsRecord => state.get("sortOptions");

export const getSortedRecords = createSelector(
  [ getSortOptions, getActiveSpace, getSpaces ],
  (sortOptions: SortOptionsRecord, activeSpace: string | null, spaces: Map<string, SpaceRecord>): List<RecordTypes> => {
    if (!activeSpace) {
      return List();
    }

    const space = spaces.get(activeSpace);

    if (space === undefined) {
      return List();
    }

    // @ts-ignore
    const records = space.get(sortOptions.get("table")).sort((recordA: any, recordB: any): number => {
      const aColumn = recordA.get(camelCase(sortOptions.get("column")));
      const bColumn = recordB.get(camelCase(sortOptions.get("column")));

      if (isMoment(aColumn) && isMoment(bColumn)) {
        if (aColumn.isAfter(bColumn)) {
          return -1;
        }

        if (aColumn.isBefore(bColumn)) {
          return 1;
        }

        return 0;
      }

      if (aColumn < bColumn) {
        return -1;
      }

      if (aColumn > bColumn) {
        return 1;
      }

      return 0;
    });

    return sortOptions.get("order") === Order.DESC ? records.reverse() : records;
  }
);
