import { Map, Record } from "immutable";
import { SortOptions, SortOptionsRecord, SpaceRecord } from "./types";

interface IState {
  activeSpace: string | null;
  loading: boolean;
  spaces: Map<string, SpaceRecord>;
  sortOptions: SortOptionsRecord;
  errors: Map<string, Map<string, string>>;
}

export type StateRecord = Record<IState>;

export const State = Record<IState>({
  activeSpace: null,
  loading: true,
  sortOptions: new SortOptions(),
  spaces: Map(),
  errors: Map(),
});
