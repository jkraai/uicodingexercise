import { List, Record } from "immutable";
import { Moment } from "moment";

/**
 * Interfaces
 * These interfaces are not exported since they shouldn't be used to
 * type hint outside of this file.
 * The primary purpose of these is as a generic argument to
 * an Immutable Record.
 */
interface IEntry {
  title: string | null;
  summary: string | null;
  createdBy: string | null;
  updatedBy: string | null;
  lastUpdated: Moment | null;
}

interface IAsset {
  title: string | null;
  contentType: string | null;
  fileName: string | null;
  createdBy: string | null;
  updatedBy: string | null;
  lastUpdated: Moment | null;
}

interface ISpace {
  id: string | null;
  name: string | null;
  createdBy: string | null;
  description: string | null;
  assets: List<AssetRecord>;
  entryRecords: List<EntryRecord>;
}

interface ISortOptions {
  table: Table;
  column: string;
  order: Order;
}

export enum Order {
  ASC = "asc",
  DESC = "desc",
}

export enum Table {
  ASSETS = "assets",
  ENTRIES = "entryRecords",
}

/**
 * Record Types
 */
export type EntryRecord = Record<IEntry>;
export type AssetRecord = Record<IAsset>;
export type SpaceRecord = Record<ISpace>;

export type SortOptionsRecord = Record<ISortOptions>;

/**
 * Record Factories
 */
export const Entry = Record<IEntry>({
  title: null,
  summary: null,
  createdBy: null,
  updatedBy: null,
  lastUpdated: null,
});
export const Asset = Record<IAsset>({
  title: null,
  contentType: null,
  fileName: null,
  createdBy: null,
  updatedBy: null,
  lastUpdated: null,
});

export const Space = Record<ISpace>({
  assets: List(),
  createdBy: null,
  description: null,
  entryRecords: List(),
  id: null,
  name: null,
});

export const SortOptions = Record<ISortOptions>({
  column: "title",
  order: Order.ASC,
  table: Table.ENTRIES,
});
